"Openhab MALSOW"  

* Récupération du projet : 

- git clone https://gitlab.com/mbds/maslow/openhab-maslow.git

* Lancement du projet : 

- dans le dossier openhab-maslow : lancer le fichier "start.bat"
- aller sur votre navigateur préféré et taper : http://localhost:8080/
- choisir basicui -> Prise -> Maison -> Prise connectée 
- changer l'état de votre prise connctée !